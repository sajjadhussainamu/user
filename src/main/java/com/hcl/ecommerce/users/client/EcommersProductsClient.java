package com.hcl.ecommerce.users.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hcl.ecommerce.users.entity.dto.ProductResponseDto;

//@FeignClient(value = "product-service", url = "http://localhost:8092/ecommerce_product/search")
@FeignClient(name = "http://PRODUCT-SERVICE")
public interface EcommersProductsClient {

	@GetMapping("/ecommerce_product/search/product_by_name/{name}")
	public ProductResponseDto getProductByName(@PathVariable("name") String name);

	@GetMapping("/ecommerce_product/search/product_by_id/{id}")
	public ProductResponseDto getProductById(@PathVariable("id") long id);
}
