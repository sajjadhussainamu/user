package com.hcl.ecommerce.users.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hcl.ecommerce.users.entity.dto.TransactionsResponseDto;

//@FeignClient(value = "employee-service", url = "http://localhost:8081/wawa")
@FeignClient(name = "http://BANK-SERVICE")
public interface TransactionClient {

	@PostMapping("/wawa/transactions/process")
	public TransactionsResponseDto transactionsProcess(@RequestParam("formAccount") long formAccount, @RequestParam("toAccount") long toAccount,
			@RequestParam("amount") float amount);

	@GetMapping("/wawa/registraions/checkUser")
	public Boolean getCustomerByUsername(@RequestParam("username") String username);

}
