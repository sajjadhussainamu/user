package com.hcl.ecommerce.users.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ecommerce.users.client.EcommersProductsClient;
import com.hcl.ecommerce.users.client.TransactionClient;
import com.hcl.ecommerce.users.entity.User;
import com.hcl.ecommerce.users.entity.dto.MyOrdersResponseDto;
import com.hcl.ecommerce.users.entity.dto.ProductResponseDto;
import com.hcl.ecommerce.users.service.UserOrderService;
import com.hcl.ecommerce.users.service.UserSerchService;

@RestController
@RequestMapping("orders")
public class UserOrderController {

	@Autowired
	UserSerchService userSerchService;

	@Autowired
	TransactionClient transactionClient;

	@Autowired
	EcommersProductsClient ecommersProductsClient;

	@Autowired
	UserOrderService userOrderService;

	@PostMapping
	public MyOrdersResponseDto requestingOrder(@RequestParam("userId") String userId,
			@RequestParam("fromAccount") Long fromAccount, @RequestParam("productId") Long productId) {
		long toAccount = 2228725018692798l;
		float amount = 500f;
		MyOrdersResponseDto myOrdersResponseDto = new MyOrdersResponseDto();
		Optional<User> user = userOrderService.getUserByUserId(userId);

		if (!user.isPresent()) {
			myOrdersResponseDto.setStatusCode(400);
			myOrdersResponseDto.setStatusMessage("user is not available");
			return myOrdersResponseDto;
		}

		ProductResponseDto productResponseDto = ecommersProductsClient.getProductById(productId);
		if (productResponseDto.getProductDtos().size() == 0) {
			myOrdersResponseDto.setStatusCode(400);
			myOrdersResponseDto.setStatusMessage("product has not available yet");
			return myOrdersResponseDto;
		}

		myOrdersResponseDto = userOrderService.placedOrder(fromAccount, toAccount, amount, productId,user.get().getUserId());

		return myOrdersResponseDto;
	}

}
