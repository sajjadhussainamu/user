package com.hcl.ecommerce.users.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ecommerce.users.entity.dto.MyOrdersResponseDto;
import com.hcl.ecommerce.users.entity.dto.ProductResponseDto;
import com.hcl.ecommerce.users.service.UserSerchService;

@RestController
@RequestMapping("search")
public class UserSerchController {

	@Autowired
	UserSerchService userSerchService;

	@GetMapping("/product_by_name/{name}")
	public ProductResponseDto getProductsClientByName(@PathVariable("name") String name) {

		return userSerchService.getProductsClientByName(name);
	}

	@GetMapping("/product_by_id/{id}")
	public ProductResponseDto getProductsClientById(@PathVariable("id") long id) {

		ProductResponseDto productResponseDto = userSerchService.getProductsClientById(id);

		return productResponseDto;
	}

	@GetMapping("/myOrders/{userId}")
	public MyOrdersResponseDto getMyOrdersByUserId(@PathVariable("userId") String userId) {

		MyOrdersResponseDto responseDto = userSerchService.getMyOrdersByUserId(userId);

		return responseDto;
	}

}
