package com.hcl.ecommerce.users.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MyOrdersDto {

	private long orderId;

	private long productId;

	private float productAmount;
	
	private String productName;

}
