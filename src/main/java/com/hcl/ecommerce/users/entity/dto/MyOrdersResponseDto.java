package com.hcl.ecommerce.users.entity.dto;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MyOrdersResponseDto {

	private int statusCode;
	private String statusMessage;

	List<MyOrdersDto> myOrdersDtos = new ArrayList<>();

}
