package com.hcl.ecommerce.users.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ProductDto {

	private long id;

	private String productName;

	private long categoryId;
}
