package com.hcl.ecommerce.users.entity.dto;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class TransactionResponseDto {

	private long id;
	private long fromAccount;
	private long toAccount;
	private float amount;

}
