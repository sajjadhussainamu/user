package com.hcl.ecommerce.users.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.ecommerce.users.entity.MyOrder;

@Repository
public interface UserOrderRepository extends JpaRepository<MyOrder, Long> {

	Optional<MyOrder> findByUserId(String id);

}
