package com.hcl.ecommerce.users.service;

import java.util.Optional;

import com.hcl.ecommerce.users.entity.User;
import com.hcl.ecommerce.users.entity.dto.MyOrdersResponseDto;

public interface UserOrderService {

	
	Optional<User> getUserByUserId(String userId);

	MyOrdersResponseDto placedOrder(Long fromAccount, long toAccount, float amount, Long productId, String userId);

}
