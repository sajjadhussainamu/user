package com.hcl.ecommerce.users.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.ecommerce.users.client.TransactionClient;
import com.hcl.ecommerce.users.entity.MyOrder;
import com.hcl.ecommerce.users.entity.User;
import com.hcl.ecommerce.users.entity.dto.MyOrdersDto;
import com.hcl.ecommerce.users.entity.dto.MyOrdersResponseDto;
import com.hcl.ecommerce.users.entity.dto.TransactionsResponseDto;
import com.hcl.ecommerce.users.repository.UserOrderRepository;
import com.hcl.ecommerce.users.repository.UserRepository;

@Service
@Transactional
public class UserOrderServiceImpl implements UserOrderService {

	@Autowired
	TransactionClient transactionClient;

	@Autowired
	UserOrderRepository userOrderRepository;

	@Autowired
	UserRepository userRepository;

	@Override
	public MyOrdersResponseDto placedOrder(Long fromAccount, long toAccount, float amount, Long productId,
			String userId) {

		TransactionsResponseDto transactionsProcess = transactionClient.transactionsProcess(fromAccount, toAccount,
				amount);
		MyOrdersResponseDto myOrdersResponseDto = new MyOrdersResponseDto();
		if (transactionsProcess.getStatusCode() != 200) {
			myOrdersResponseDto.setStatusCode(transactionsProcess.getStatusCode());
			myOrdersResponseDto.setStatusMessage(transactionsProcess.getStatusMessage());
			return myOrdersResponseDto;
		}
		MyOrder myOrder = new MyOrder();
		myOrder.setProductId(productId);
		myOrder.setProductAmount(amount);
		myOrder.setUserId(userId);
		userOrderRepository.save(myOrder);
		MyOrdersDto myOrderDto = new MyOrdersDto();

		BeanUtils.copyProperties(myOrder, myOrderDto);
		List<MyOrdersDto> myOrdersDto = new ArrayList<>();
		myOrdersDto.add(myOrderDto);
		myOrdersResponseDto.setStatusCode(transactionsProcess.getStatusCode());
		myOrdersResponseDto.setStatusMessage("Order has successfully done Your Order is");
		myOrdersResponseDto.setMyOrdersDtos(myOrdersDto);

		return myOrdersResponseDto;
	}

	@Override
	public Optional<User> getUserByUserId(String userId) {
		return userRepository.findByUserId(userId);

	}


}
