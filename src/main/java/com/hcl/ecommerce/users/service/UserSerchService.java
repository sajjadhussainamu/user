package com.hcl.ecommerce.users.service;

import com.hcl.ecommerce.users.entity.dto.MyOrdersResponseDto;
import com.hcl.ecommerce.users.entity.dto.ProductResponseDto;

public interface UserSerchService {

	ProductResponseDto getProductsClientByName(String name);

	ProductResponseDto getProductsClientById(long id);


	MyOrdersResponseDto getMyOrdersByUserId(String userId);

}
