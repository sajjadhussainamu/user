package com.hcl.ecommerce.users.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.ecommerce.users.client.EcommersProductsClient;
import com.hcl.ecommerce.users.entity.MyOrder;
import com.hcl.ecommerce.users.entity.dto.MyOrdersDto;
import com.hcl.ecommerce.users.entity.dto.MyOrdersResponseDto;
import com.hcl.ecommerce.users.entity.dto.ProductResponseDto;
import com.hcl.ecommerce.users.repository.UserOrderRepository;

@Service
public class UserSerchServiceImpl implements UserSerchService {

	@Autowired
	EcommersProductsClient ecommersProductsClient;

	@Autowired
	UserOrderRepository userOrderRepository;

	@Override
	public ProductResponseDto getProductsClientByName(String name) {
		ProductResponseDto productResponseDto = ecommersProductsClient.getProductByName(name);
		return productResponseDto;
	}

	@Override
	public ProductResponseDto getProductsClientById(long id) {
		ProductResponseDto productResponseDto = ecommersProductsClient.getProductById(id);
		return productResponseDto;
	}

	@Override
	public MyOrdersResponseDto getMyOrdersByUserId(String userId) {
		Optional<MyOrder> myOrder = userOrderRepository.findByUserId(userId);
		MyOrdersResponseDto myOrdersResponseDto = new MyOrdersResponseDto();
		if (!myOrder.isPresent()) {
			myOrdersResponseDto.setStatusCode(200);
			myOrdersResponseDto.setStatusMessage("You have no Order");
			return myOrdersResponseDto;
		}
		ProductResponseDto productResponseDto = ecommersProductsClient.getProductById(myOrder.get().getProductId());
		MyOrdersDto myOrdersDto = new MyOrdersDto();
		
		BeanUtils.copyProperties(myOrder.get(), myOrdersDto);
		
		List<MyOrdersDto> myOrdersDtos = new ArrayList<>();
		myOrdersDto.setProductName(productResponseDto.getProductDtos().get(0).getProductName());
		myOrdersDtos.add(myOrdersDto);
		
		myOrdersResponseDto.setMyOrdersDtos(myOrdersDtos);
		myOrdersResponseDto.setStatusCode(200);
		myOrdersResponseDto.setStatusMessage("Find below your order list");
		return myOrdersResponseDto;

	}

}
